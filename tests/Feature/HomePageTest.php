<?php

namespace Tests\Feature;

use Tests\TestCase;

class HomePageTest extends TestCase
{
	public function testCanAccessTheHomePage()
	{
		$this->get('/')
			->assertStatus(200)
			->assertViewIs('welcome');
	}

	public function testSeesTheRightTextInHomePage()
	{
		$this->get('/')
			->assertSee('DevOps Hello World!')
			->assertSee('<title>DevOps Hello World</title>')
			->assertDontSee('Docs')
			->assertDontSee('Laracasts')
			->assertDontSee('News')
			->assertDontSee('Blog')
			->assertDontSee('Nova')
			->assertDontSee('Forge')
			->assertDontSee('GitHub');
	}
}
